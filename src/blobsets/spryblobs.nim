import asyncdispatch
import ../blobsets, ./filestores, ./httpstores
import spryvm/spryvm
import cbor
import std/strutils, std/tables, std/os

type BlobIdSpryNode* = ref object of Value
  id*: BlobId

type BlobSetSpryNode* = ref object of Value
  set*: BlobSet

method eval*(self: BlobIdSpryNode; spry: Interpreter): Node = self
method eval*(self: BlobSetSpryNode; spry: Interpreter): Node = self

method `$`*(self: BlobIdSpryNode): string = $self.id
method `$`*(self: BlobSetSpryNode): string = "<BlobSet>"

type BlobStoreNode* = ref object of Value
  ## Object for store interface and caches
  store: BlobStore
  blobs: Table[string, tuple[id: BlobId, size: BiggestInt]]
  sets: Table[string, BlobSet]

proc insertPath(store: BlobStore; bs: BlobSet; kind: PathComponent; path: string): BlobSet =
  result = bs
  case kind
  of pcFile, pcLinkToFile:
    var path = normalizedPath path
    let (id, size) = waitFor store.ingestFile(path)
    path.removePrefix(getCurrentDir())
    path.removePrefix("/")
    result = waitFor insert(store, result, path, id, size)
  of pcDir, pcLinkToDir:
    for kind, subPath in path.walkDir:
      result = store.insertPath(result, kind, subPath)

proc getSet(env: BlobStoreNode; path: string): BlobSet =
  result = env.sets.getOrDefault(path)
  if result.isNil:
    result = newBlobSet()
    result = env.store.insertPath(result, path.getFileInfo.kind, path)
    if not result.isEmpty:
      env.sets[path] = result

proc ingestBlobs(env: BlobStoreNode; path: string): BlobSetSpryNode =
  let bs = env.getSet(path)
  BlobSetSpryNode(set: bs)

# Spry Blobset module
proc addBlobSets*(spry: Interpreter; store: BlobStore) =

  let env = BlobStoreNode(
    store: store,
    blobs: initTable[string, tuple[id: BlobId, size: BiggestInt]](),
    sets: initTable[string, BlobSet]())

  nimMeth "blobMerge":
    let
      a = BlobSetSpryNode(evalArgInfix(spry))
      b = BlobSetSpryNode(evalArg(spry))
      c = env.store.union(a.set, b.set)
    BlobSetSpryNode(set: c)

  nimFunc "blobLoad":
    let
      node = BlobIdSpryNode(evalArg(spry))
      set = waitFor env.store.load(node.id)
    BlobSetSpryNode(set: set)

  nimFunc "blobIngest":
    let path = StringVal(evalArg(spry)).value
    result = ingestBlobs(env, path)

  nimFunc "toBlobId":
    let str = StringVal(evalArg(spry)).value
    BlobIdSpryNode(id: str.toBlobId)

  nimFunc "blobCbor":
    let set = BlobSetSpryNode(evalArg(spry)).set
    newValue($toCbor(set))

  nimFunc "blobCommit":
    let
      setNode = BlobSetSpryNode(evalArg(spry))
      coldSet = waitFor env.store.commit(setNode.set)
    BlobIdSpryNode(id: coldSet.setId)

  nimFunc "blobHex":
    let id = BlobIdSpryNode(evalArg(spry)).id
    newValue(id.toHex)

  nimFunc "key":
    let str = StringVal(evalArg(spry)).value
    newValue(str.toKey.int)
