# Package

version       = "0.2.1"
author        = "Emery Hemingway"
description   = "Sets of named blobs"
license       = "AGPLv3"
srcDir        = "src"

requires "nim >= 0.18.0", "base32", "cbor >= 0.5.2", "siphash", "tiger >= 0.2.1"

bin = @["blobingest", "blobset", "blobserver"]
skipFiles = @["blobset.nim", "blobserver.nim"]
