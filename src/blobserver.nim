import std/asyncdispatch
import ./blobsets, ./blobsets/filestores, ./blobsets/httpservers

proc main(): Future[void] =
  let
    store = newFileStore("")
    server = newHttpStoreServer(store)
  server.serve((Port)8080)

waitFor main()
