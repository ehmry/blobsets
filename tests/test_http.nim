import std/asyncdispatch, std/net, std/random, std/strutils, std/unittest

import ../src/blobsets, ../src/blobsets/filestores, ../src/blobsets/httpstores, ../src/blobsets/httpservers

let
  port = (Port)rand(1 shl 15)
  store = newFileStore("/tmp/store")
  server = newHttpStoreServer(store)
asyncCheck server.serve(port)

let
  url = "http://127.0.0.1:$1/" % $port
  client = newHttpStore url

suite "Http store":
  randomize()

  var
    blob: BlobId
    size: BiggestInt

  test "ingest":
    (blob, size) = waitFor client.ingestFile("tests/test_http.nim")
  test "dump":
    for chunk in store.dumpBlob(blob):
      discard chunk

  test "ingest":
    (blob, size) = waitFor client.ingestFile("tests/test_http.nim")

const count = 64
var
  setId: SetId
  bs: BlobSet

suite "store":
  var rng = initRand(rand(int.high))

  test "commit":
    bs = newBlobSet()
    for i in 1..count:
      let
        name = $i
        blob = waitFor client.ingest(newString(i))
      bs = waitFor insert(client, bs, name, blob, i)
    setId = (waitFor commit(client, bs)).setId

  test "load":
    bs = waitFor client.load(setId)
    for i in 1..count:
      let
        name = $i
        blob = blobHash newString(i)
      var found = false
      apply(client, bs, name) do (id: BlobId; size: BiggestInt):
        check(id == blob)
        found = true
      check(found)
    for i in 1..count:
      let
        i = i and 0x8000
        name = $i
      apply(client, bs, name) do (id: BlobId; size: BiggestInt):
        echo "inserted ", name, " - ", name.toKey
        echo "applied ", name, " - ", ($(i xor 0x8000)).toKey
        raiseAssert("apply succedded for a key not inserted")

  test "random":
    for i in 1..count:
      store.randomApply(bs, rng) do (id: BlobId; size: BiggestInt):
        let stream = store.openBlobStream(id, size, dataBlob)
        close stream

  test "stream":
    proc findAll() {.async.} =
      for i in 1..count:
        let stream = newMemberStream()
        asyncCheck stream.streamMembers(store, bs)
        var
          found = false
          count = 0
        while not found:
          let (valid, val) = await stream.read()
          if not valid: break
          if i == val.size:
            complete stream
            found = true
          inc count
        check(count > 0)
        check(found)
    waitFor findAll()
