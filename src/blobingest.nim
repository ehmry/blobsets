when not isMainModule:
  {.error: "this module is not a library, import blobsets instead".}

import std/asyncdispatch, std/os, std/strutils, std/tables, std/parseopt, std/streams, std/rdstdin, std/random
import cbor
import ./blobsets, ./blobsets/filestores,
  ./blobsets/httpservers, ./blobsets/httpstores

proc openStore(): BlobStore =
  const key = "BLOB_STORE_URL"
  var url = os.getEnv key
  if url == "":
    url = "/tmp/blobs"
    #quit(key & " not set in environment")
  if url.startsWith "http://":
    newHttpStore(url)
  else:
    newFileStore(url)

proc insertPath(store: BlobStore; bs: BlobSet; kind: PathComponent; path: string): BlobSet =
  result = bs
  try:
    case kind
    of pcFile, pcLinkToFile:
      var path = normalizedPath path
      let (id, size) = waitFor store.ingestFile(path)
      path.removePrefix(getCurrentDir())
      path.removePrefix("/")
      result = waitfor insert(store, result, path, id, size)
      writeLine(stdout, id, align($size, 11), " ", path)
    of pcDir, pcLinkToDir:
      for kind, subPath in path.walkDir:
        result = store.insertPath(result, kind, subPath)
  except:
    let e = getCurrentException()
    writeLine(stderr, "failed to ingest '", path, "', ", e.msg)

proc ingestMain*() {.async.} =
  var args = newSeq[string]()
  for kind, key, val in getopt():
    if kind == cmdArgument:
      args.add key
  if 0 < args.len:
    var set = newBlobSet()
    let store = openStore()
    for i in 0..args.high:
      let path = normalizedPath args[i]
      set = store.insertPath(set, path.getFileInfo.kind, path)
    let final = await store.commit(set)
    writeLine(stdout, final.setId.toBase32)

when isMainModule:
  waitFor ingestMain()
