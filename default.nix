with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "blobsets-0";

  src = ./.;

  base32 = fetchFromGitHub {
    owner = "ehmry";
    repo = "base32.nim";
    rev = "c557fd9e1d09d103f3781b6eba5fa22330579425";
    sha256 = "081rlflad99f4krlsfgll6f5l09w3a2i4zgfwncxya5hn6jhfg32";
  };

  cbor = fetchFromGitHub {
    owner = "ehmry";
    repo = "nim-cbor";
    rev = "v0.6.0";
    sha256 = "175h3vk0qccmzymlbqdwni645d7v5zz0fn6zdjsy1pd88kg5v66h";
  };

  siphash = fetchFromGitHub {
    owner = "ehmry";
    repo = "nim-siphash";
    rev = "v0.1.0";
    sha256 = "1z4xm3ckygw8pmn9b6axdk65ib1y5bgwa3v1dbxamvhjmyfr62w4";
  };

  tiger = fetchFromGitHub {
    owner = "ehmry";
    repo = "tiger";
    rev = "v0.2";
    sha256 = "0hn3ccmmfgkmiz5q2mvlyhgc22054i54m211ai2a9k9k37w7w2hz";
  };

  buildInputs = [ nim pcre ];

  NIX_LDFLAGS = [ "-lpcre" ];

  buildPhase = ''
    HOME=$TMPDIR
    nim c -p:$base32 -p:$cbor/src -p:$siphash/src -p:$tiger -d:release src/blobset.nim
  '';

  installPhase = "install -Dt $out/bin src/blobset";
}
