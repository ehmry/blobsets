type
  RepBlobStream = ref RepBlobStreamObj
  RepBlobStreamObj = object of BlobStreamObj
    src: BlobStream
    dst: IngestStream
    id: BlobId
    size: BiggestInt
    ingestPos: BiggestInt

proc repClose(s: BlobStream) =
  var s = (RepBlobStream)s
  close s.src
  cancel s.dst

proc finish(s: RepBlobStream) =
  var buf = ""
  while s.ingestPos < s.size:
    buf.setLen(min(blobLeafSize, s.size - s.ingestPos))
    let n = read(s.src, buf, buf.len)
    ingest(s.dst, buf, n)
    inc s.ingestPos, n

proc repSetPos(s: BlobStream; pos: BiggestInt) =
  var s = (RepBlobStream)s
  if pos != ingestPos and s.ingestPos < s.size:
    finish s
  s.src.pos = pos

proc repGetPos(s: BlobStream): BiggestInt =
  var s = (RepBlobStream)s
  s.src.pos

proc repRead(s: BlobStream; buffer: pointer; bufLen: int): int =
  var s = (RepBlobStream)s
  result = read(s.src, buffer, bufLen)
  if s.ingestPos < s.size:
    if bufLen < blobLeafSize:
      finish s
    else:
      ingest(s.dst, buffer, result)

type
  RepStore = ref RepStoreObj
  RepStoreObj = object of BlobStoreObj
    src, dst: BlobStore

proc replicatorOpenBlobStream(store: BlobStore; id: BlobId; size: BiggestInt; kind: BlobKind) =
  var
    store = (RepStore)store
  try:
    result = openBlobStream(store.dst, id, size, kind)
  except KeyError:
    let
      src = openBlobStream(store.src, id, size, kind)
      dst = openIngestStream(store.dst, size, kind)
      size = if 0 < size: size else: src.size
    result = RepStore(
      closeImpl: repClose,
      setPosImpl: repSetPos,
      getPosImpl: repGetPos,
      readImpl: repRead,
      dst: src, dst: dst, id: id, size: size
    )

proc replicatorOpenIngestStream(s: BlobStore; size: BiggestInt; kind: BlobKind) =
  openIngestStream(RepStore(store).dst)

proc replicatorCloseStore(s: BlobStore) =
  var s = (RepStore)s
  close s.src
  close s.dst

proc newReplicatorStore*(src, dst: Store): BlobStore =
  RepStore(
    closeImpl: replicatorCloseStore,
    openBlobStreamImpl: replicatorOpenBlobStream,
    openIngestStreamImpl: replicatorOpenIngestStream,
    src: src, dst: dst,
  )
