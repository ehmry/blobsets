# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "Blobset components for Genode"
license       = "AGPLv3"
srcDir        = "src"
binDir        = "bin"
bin           = @[
  "blobsets_fs",
  "blobsets_http",
  "blobsets_rom",
]
backend       = "cpp"

# Dependencies

requires "nim >= 0.19.0", "blobsets", "genode"

task genode, "Build for Genode":
  exec "nimble build --os:genode -d:posix"
