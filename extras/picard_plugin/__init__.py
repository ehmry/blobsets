PLUGIN_NAME = 'Blobsets'
PLUGIN_AUTHOR = 'ehmry'
PLUGIN_DESCRIPTION = '''Ingest files into a blobset.'''
PLUGIN_VERSION = '0.1'
PLUGIN_API_VERSIONS = ['2.0']
PLUGIN_LICENSE = "GPL-2.0"
PLUGIN_LICENSE_URL = "https://www.gnu.org/licenses/gpl-2.0.html"

from functools import partial

from picard.config import TextOption
from picard.file import File
from picard.album import Album
from picard.track import Track
from picard.ui.itemviews import BaseAction, register_track_action, register_album_action
from picard.ui.options import register_options_page, OptionsPage
from picard.util import thread
from .ui_options_blobsets import Ui_BlobsetsOptionsPage

import picard.tagger as tagger

from PyQt5 import QtWidgets

from subprocess import Popen, PIPE
import os.path

ROOT_HASH_KEY = "blobsets_root_hash"

# TODO the setttings thing cannot be trusted

class BlobsetsOptionsPage(OptionsPage):
    NAME = 'blobsets'
    TITLE = 'Blobsets'
    PARENT = "plugins"
    options = [
        TextOption("setting", ROOT_HASH_KEY, "d59ea2fbcbf2f3d21184f21938ed7966aa480c1dea7463b3de6cae1a18bb1308")
    ]

    def __init__(self, parent=None):
        super(BlobsetsOptionsPage, self).__init__(parent)
        self.ui = Ui_BlobsetsOptionsPage()
        self.ui.setupUi(self)

    def load(self):
        self.ui.root_hash.setText(self.config.setting[ROOT_HASH_KEY])

    def save(self):
        self.config.setting[ROOT_HASH_KEY] = self.ui.root_hash.text()

register_options_page(BlobsetsOptionsPage)

class IngestFile(BaseAction):
    NAME = 'Ingest file to blob set'

    def __init__(self):
        super(IngestFile, self).__init__()
        register_track_action(self)
        self.root_sym = 0
        self.blobset_process = Popen(["blobset", "repl"], stdin=PIPE, stdout=PIPE)
        root_hex = tagger.config.setting[ROOT_HASH_KEY]
        cmd = '(define root_{} (load {}))\n'.format(self.root_sym, root_hex)
        print(cmd)
        self.blobset_process.stdin.write(cmd.encode(encoding='UTF-8'))
        self.blobset_process.stdin.flush()
        print(self.blobset_process.stdout.readline().decode())

    def _ingest_tracks(self, objs):
        for track in objs:
            if isinstance(track, Track):
                for file in track.linked_files:
                    mbid = track.metadata["musicbrainz_recordingid"]
                    path = file.filename
                    self.tagger.window.set_statusbar_message(
                        N_('Ingesting %(path)s...'), {'path': path})
                    _, ext = os.path.splitext(path)
                    cmd = '(define root_{} (insert root_{} (blob (path "{}")) "{}{}"))\n'.format(self.root_sym+1, self.root_sym, path, mbid, ext)
                    print(cmd)
                    self.blobset_process.stdin.write(cmd.encode(encoding='UTF-8'))
                    self.blobset_process.stdin.flush()
                    self.tagger.window.set_statusbar_message(self.blobset_process.stdout.readline().decode())
                    self.root_sym = self.root_sym+1
                    break
        cmd = '(hex (commit root_{}))\n'.format(self.root_sym)
        print(cmd)
        self.blobset_process.stdin.write(cmd.encode(encoding='UTF-8'))
        self.blobset_process.stdin.flush()
        output = self.blobset_process.stdout.readline()
        new_root = output.decode().strip().strip('"')
        print("new root is", new_root)
        tagger.config.setting[ROOT_HASH_KEY] = new_root
        self.tagger.window.set_statusbar_message(
            N_('Root hash updated to %(hash)s.'),
            {'hash': tagger.config.setting[ROOT_HASH_KEY]}
        )

    def _ingest_callback(self, result=None, error=None):
        if error:
            self.tagger.window.set_statusbar_message('ingestion failed.')

    def callback(self, objs):
        # communicate with the REPL on the save thread
        thread.run_task(
            partial(self._ingest_tracks, objs),
            partial(self._ingest_callback),
            priority=2, thread_pool=self.tagger.save_thread_pool)

class CopyIdToClipboard(BaseAction):
    NAME = "Copy id Clipboard..."

    # Copy an id to the clipboard so the playlist generator picks it up

    def callback(self, objs):
        if len(objs) != 1:
            return
        if isinstance(objs[0], Track):
          track = objs[0]
          mbid = track.metadata["musicbrainz_recordingid"]
          clipboard = QtWidgets.QApplication.clipboard()
          clipboard.setText('musicbrainz/recording/{}'.format(mbid))
        elif isinstance(objs[0], Album):
          album = objs[0]
          mbid = album.metadata["musicbrainz_albumid"]
          clipboard = QtWidgets.QApplication.clipboard()
          clipboard.setText('musicbrainz/release/{}'.format(mbid))

IngestFile()

register_track_action(CopyIdToClipboard())
register_album_action(CopyIdToClipboard())
