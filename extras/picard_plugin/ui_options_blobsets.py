from PyQt5 import QtCore, QtWidgets


class Ui_BlobsetsOptionsPage(object):

    def setupUi(self, BlobsetsOptionsPage):
        BlobsetsOptionsPage.setObjectName("BlobsetsOptionsPage")
        self.vboxlayout = QtWidgets.QVBoxLayout(BlobsetsOptionsPage)
        self.vboxlayout.setContentsMargins(9, 9, 9, 9)
        self.vboxlayout.setSpacing(6)
        self.vboxlayout.setObjectName("vboxlayout")
        self.root_hash_group = QtWidgets.QGroupBox(BlobsetsOptionsPage)
        self.root_hash_group.setObjectName("root_hash_group")
        self.vboxlayout2 = QtWidgets.QVBoxLayout(self.root_hash_group)
        self.vboxlayout2.setContentsMargins(9, 9, 9, 9)
        self.vboxlayout2.setSpacing(2)
        self.vboxlayout2.setObjectName("vboxlayout2")
        self.root_hash_label = QtWidgets.QLabel(self.root_hash_group)
        self.root_hash_label.setObjectName("root_hash_label")
        self.vboxlayout2.addWidget(self.root_hash_label)
        self.root_hash = QtWidgets.QLineEdit(self.root_hash_group)
        self.root_hash.setObjectName("root_hash")
        self.vboxlayout2.addWidget(self.root_hash)
        self.vboxlayout.addWidget(self.root_hash_group)
        spacerItem = QtWidgets.QSpacerItem(1, 1, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.vboxlayout.addItem(spacerItem)

        self.retranslateUi(BlobsetsOptionsPage)
        QtCore.QMetaObject.connectSlotsByName(BlobsetsOptionsPage)

    def retranslateUi(self, BlobsetsOptionsPage):
        self.root_hash_label.setText(_("Current Blobset root hash"))
