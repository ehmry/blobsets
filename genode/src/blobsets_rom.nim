import std/asyncdispatch
import std/xmltree, std/streams, std/strtabs, std/strutils, std/xmlparser, std/tables
import genode, genode/parents, genode/servers, genode/roms
import blobsets, blobsets/filestores

const
  currentPath = currentSourcePath.rsplit("/", 1)[0]
  header = currentPath & "/private/rom_component.h"

type
  RomSessionCapability {.
    importcpp: "Genode::Rom_session_capability",
    header: "<rom_session/capability.h>".} = object

  RomSessionComponentBase {.importcpp, header: header.} = object
    impl {.importcpp.}: pointer
  RomSessionComponent = Constructible[RomSessionComponentBase]

  Session = ref object
    env: GenodeEnv
    cpp: RomSessionComponent
    ds: DataspaceCapability

proc isValid(cap: RomSessionCapability): bool {.importcpp: "#.valid()".}

proc deliverSession*(parent: Parent; id: ServerId; cap: RomSessionCapability) {.
  importcpp: "#->deliver_session_cap(Genode::Parent::Server::Id{#}, #)".}

proc newSession(env: GenodeEnv; ds: DataspaceCapability): Session =
  proc construct(cpp: RomSessionComponent; ds: DataspaceCapability) {.importcpp.}
  new result
  result.env = env
  result.cpp.construct(ds)
  result.ds = ds

proc manage(ep: Entrypoint; s: Session): RomSessionCapability =
  proc manage(ep: Entrypoint; cpp: RomSessionComponent): RomSessionCapability {.
    importcpp: "#.manage(*#)".}
  result = ep.manage(s.cpp)
  GC_ref s

proc dissolve(s: Session) =
  proc dissolve(ep: Entrypoint; cpp: RomSessionComponent) {.
    importcpp: "#.dissolve(*#)".}
  let
    ep = s.env.ep
    pd = s.env.pd
  ep.dissolve(s.cpp)
  destruct s.cpp
  pd.freeDataspace s.ds
  GC_unref s

componentConstructHook = proc(env: GenodeEnv) =
  var
    store = newFileStore("/store")
    policies = newSeq[XmlNode](8)
    sessions = initTable[ServerId, Session]()

  proc readDataspace(romSet: SetId; label: string): DataspaceCapability =
    let
      name = label.lastLabelElement
      bs = waitFor store.load romSet
    var cap: DataspaceCapability
    store.apply(bs, name) do (id: BlobId; size: BiggestInt):
      let
        pd = env.pd
        cap = pd.allocDataspace(size.int)
        fact = env.rm.newDataspaceStreamFactory(cap)
        stream = fact.newStream()
      for chunk in store.dumpBlob(id):
        stream.write(chunk)
      close stream
      close fact
    if cap.isValid:
      result = cap
    else:
      echo name, " not in set ", romSet, " for '", label, "'"

  proc createSession(env: GenodeEnv; id: ServerId; label: string; romSet: SetId) =
    let romDs = readDataspace(romSet, label)
    if romDs.isValid:
      let session = env.newSession(romDs)
      sessions[id] = session
      let sessionCap = env.ep.manage(session)
      echo "deliver ROM to ", label
      env.parent.deliverSession id, sessionCap
    else:
      echo "deny ROM to ", label
      let parent = env.parent
      parent.sessionResponseDeny(id)

  proc processConfig(rom: RomClient) =
    update rom
    policies.setLen 0
    let
      configXml = rom.xml
    configXml.findAll("default-policy", policies)
    if policies.len > 1:
      echo "more than one '<default-policy/>' found, ignoring all"
      policies.setLen 0
    configXml.findAll("policy", policies)

  proc processSessions(rom: RomClient) {.gcsafe.} =
    update rom
    var requests = initSessionRequestsParser(rom)
  
    for id in requests.close:
      if sessions.contains id:
        let s = sessions[id]
        dissolve(s)
        sessions.del id
        env.parent.sessionResponseClose(id)
  
    for id, label, args in requests.create "ROM":
      try:
        let policy = policies.lookupPolicy label
        doAssert(not sessions.contains(id), "session already exists for id")
        doAssert(label != "")
        if policy.isNil:
          echo "no policy matched '", label, "'"
          env.parent.sessionResponseDeny(id)
        else:
          var romSet: SetId
          let pAttrs = policy.attrs
          if not pAttrs.isNil and pAttrs.contains "root":
            try: romSet = toSetId pAttrs["root"]
            except ValueError: discard
          else:
            for e in label.elements:
              try:
                romSet = toSetId e
                break
              except ValueError: continue
          if romSet.isNonZero:
            createSession(env, id, label, romSet)
          else:
            echo "no valid root policy for '", label, "'"
            env.parent.sessionResponseDeny(id)
      except:
        echo "failed to create session for '", label, "', ", getCurrentExceptionMsg()
        env.parent.sessionResponseDeny(id)
    # All sessions have been instantiated and requests fired off,
    # now return to the entrypoint and dispatch packet signals,
    # which will work the chain of futures and callbacks until
    # `createSession` completes and capabilities are delivered

  let
    configRom = env.newRomHandler(
      "config", processConfig)
    sessionsRom = env.newRomHandler(
      "session_requests", processSessions)

  process configRom
  process sessionsRom

  env.parent.announce "ROM"
