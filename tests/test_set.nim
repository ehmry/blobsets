import base32
import ../src/blobsets, ../src/blobsets/filestores
import std/asyncdispatch, std/unittest, std/os, std/parseopt

suite "Encoding":
  var empty: BlobId
  test $empty:
    check empty.toBase32 == base32.encode(cast[array[digestSize,char]](empty.data), pad=false)
    check toBlobId($empty).data == empty.data
    check empty.toBase32.toBlobId.data == empty.data
    check empty.toHex.toBlobId.data == empty.data

suite "THEX":
  let store = newNullStore()
  proc testThex(data: string): string =
    writeFile "tmp", data
    let (id, _) = waitFor store.ingestFile("tmp")
    removeFile "tmp"
    id.toBase32

  test "The empty (zero-length) file":
    check testThex("") == "LWPNACQDBZRYXW3VHJVCJ64QBZNGHOHHHZWCLNQ"

  test "A file with a single zero byte":
    check testThex("\0") == "VK54ZIEEVTWNAUI5D5RDFIL37LX2IQNSTAXFKSA"

  test "A file with 1024 'A' characters":
    var s = newString(1024)
    for c in s.mitems: c = 'A'
    check testThex(s) == "L66Q4YVNAFWVS23X2HJIRA5ZJ7WXR3F26RSASFA"

  test "A file with 1025 'A' characters":
    var s = newString(1025)
    for c in s.mitems: c = 'A'
    check testThex(s) == "PZMRYHGY6LTBEH63ZWAHDORHSYTLO4LEFUIKHWY"

suite "Blob set tests":
  var
    store = newNullStore()
    randomCid = blobHash("")

  proc randomize() =
    randomCid = blobHash(randomCid.toHex)

  proc testPath(s: BlobSet; root: string): BlobSet =
    for path in walkDirRec(root):
      randomize()
      let
        blob = randomCid
        str = $randomCid
      check(str.toBlobid == randomCid)
      result = waitFor insert(store, s, path, blob, 0)
      var found = false
      apply(store, result, path) do (id: BlobId; size: BiggestInt):
        check(id == randomCid)
        found = true
      check(found)

  test "functional insert":
    let
      a = newBlobSet()
      b = waitFor insert(store, a, "foo", randomCid, 0)
      c = waitFor insert(store, b, "bar", randomCid, 0)
    check(contains(store, b, "foo"))
    check(contains(store, c, "foo"))
    check(contains(store, c, "bar"))
    check(not contains(store, a, "foo"))
    check(not contains(store, a, "bar"))
    check(not contains(store, b, "bar"))

  test "apply":
    var bs = newBlobSet()
    for i in 1..1024:
      let
        name = $i
        blob = blobHash name
      bs = waitFor insert(store, bs, name, blob, 0)
    for i in 1..1024:
      let
        name = $i
        blob = blobHash name
      apply(store, bs, name) do (id: BlobId; size: BiggestInt):
        check(id == blob)

  test "remove":
    var bs = newBlobSet()
    for i in 1..1024:
      let
        name = $i
        blob = blobHash name
      bs = waitFor insert(store, bs, name, blob, 0)
    for i in 1..1024:
      let name = $i
      bs = waitFor remove(store, bs, name)
      check(not contains(store, bs, name))

  test "sets":
    var s = newBlobSet()
    for kind, key, val in getopt():
      if kind == cmdArgument:
        s = s.testPath(key)
    if s.isEmpty:
      s = s.testPath(".")
