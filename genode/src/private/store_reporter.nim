import blobsets
import genode, genode/reports
import std/asyncdispatch, std/asyncfutures, std/streams, std/xmltree

type
  StoreReporter = ref StoreReporterObj
  StoreReporterObj = object of BlobStoreObj
    store: BlobStore
    reporter: ReportClient
    xml: XmlNode
      # TODO: put missing objects in a table and add
      # a weight value that increase for each miss

proc reportMissing(sr: StoreReporter; id: BlobId; kind: BlobKind) =
  sr.xml.add <>blob(id=id.toHex, kind=($kind))
  sr.reporter.submit do (str: Stream):
    str.writeLine(sr.xml)

proc xContains(s: BlobStore; id: BlobId; kind: BlobKind): Future[bool] {.async.} =
  var sr = StoreReporter(s)
  let r = await sr.store.containsImpl(sr.store, id, kind)
  if not r:
    sr.reportMissing(id, kind)
  return r

proc xOpenBlobStream(s: BlobStore; id: BlobId; size: BiggestInt; kind: BlobKind): BlobStream =
  var sr = StoreReporter(s)
  try:
    result = sr.store.openBlobStreamImpl(sr.store, id, size, kind)
  except KeyError:
    sr.reportMissing(id, kind)
    raise newException(KeyError, "blob missing blob reported")

proc xOpenIngestStream(s: BlobStore; size: BiggestInt; kind: BlobKind): IngestStream =
  var sr = StoreReporter(s)
  sr.store.openIngestStreamImpl(sr.store, size, kind)

proc newStoreReporter*(env: GenodeEnv; store: BlobStore): BlobStore =
  ## Create a new store that reports missing blobs via a Report session.
  StoreReporter(
    containsImpl: xcontains,
    openBlobStreamImpl: xopenBlobStream,
    openIngestStreamImpl: xopenIngestStream,
    store: store,
    reporter: env.newReportClient("missing"),
    xml: <>missing(),
  )
