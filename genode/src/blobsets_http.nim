import genode

import std/asyncdispatch
import blobsets/filestores, blobsets/httpservers

componentConstructHook = proc (env: GenodeEnv) =
  echo "--- Blobsets HTTP server ---"
  let server = newHttpStoreServer(newFileStore "/store")
  waitFor server.serve()
  quit "--- Blobsets HTTP server died ---"
